# Protocol V0.0

## Handshake
##### Client Request
Client sends a handshake message:
```json
{
    "protocol_version": "0.0",
    "type": "HANDSHAKE",
    "options":[
        "rate_limit",
        "binary"
    ]
}
```
The handshake message contains `protocol_version` key that declares the protocol that the client will be using. The handshake message contains optional `options` key where additional key-values can be supplied to query the server to find out if server supports those optional features. `rate_limit` and `binary` are example feature options.

##### Successful Server Response
If the server accepts the handshake, server responds:
```json
{
    "status":"ACCEPTED",
    "accepted_options":[
        "binary"
    ],
    "next_id": 0
}
```
The message contains `status` where it contains only two possible values of "ACCEPTED" and "REJECTED" (see rejection message example below). If the request message from client contains the optional `options`, the reply message will contain `accepted_options` to indicate which options are accepted. In this example, only `binary` is accepted and implying that `rate_limit` are rejected by the server as it is not present in the `accepted_options` list. Last key `next_id` contains an identification number for next request identification number to be used in next client request. Client should take that value and include it in `id` key in their request.

##### Failed Server Response (Unsupported Protocol)
In the event of server rejecting the handshake, the server responds:
```json
{
    "status":"REJECTED",
    "code": "PROTOCOL_VERSION_NOT_SUPPORTED",
    "supported":
    [
        "1.0",
        "1.1",
        "1.2"
    ]
}
```
The server contains `status` with "REJECTED" to indicate that the handshake has been rejected. The rejected message includes `code` to describe the rejection reason. In this case, "PROTOCOL_VERSION_NOT_SUPPORTED" rejection code is used to indicate that the client has submitted protocol version that the server does not support. The rejection message must include `supported` key with list of supported protocol versions.

## Submitting an one-fire non-streaming request
##### Client Request
Client sends a request message:
```json
{
    "id": 0,
    "type": "REQUEST_SINGLE",
    "resource": "temperature_sensor",
    "parameters": {
        "name": "living_room_1",
        "unit": "C"
    }
}
```
The message contains the request identification number in `id` field. The number must be unique and incremental. The number increments on every request. If the client skips or used a wrong number as id, then the connection is considered to be *desynchronized* and server will send an error message and terminate the connection. It is on the client to ensure that the request id is properly handled and is in synchronization with the server.

The message contains the mandatory `type` key. `type` key describes the type of message. In this case, "REQUEST_SINGLE" indicates that the request is a single-fire and non-streaming request. The server should understand the request and only send one data back, then the request itself is considered to be *satisfied*.

The message contains the `resource` key. `resource` key points a resource that exists on the service. The message also contains a `parameters` key that contains a map of parameters that is used to supply to the resource as arguments.

In this request example, the client is trying to access a temperature sensor information resource located at "temperature_sensor". The user has supplied two parameters, `name` and `unit` where `name` refers to the name of temperature sensor name and `unit` as a measurement unit. "living_room_1" as `name` tells the resource "temperature_sensor" to use the temperature sensor named "living_room_1" which presumedly refers a temperature sensor in a living room in the house. "C" as 'unit' tells the resource to use Celsius as a temperature unit.

##### Successful Server Response
If the server has determined that the message is valid, server responds:
```json
{
    "id": 0,
    "type": "REQUEST_SINGLE",
    "status": "ACCEPTED",
    "data": "30",
    "next_id": 1
}
```
The message includes a mandatory `id` field. The `id` field must match the request id that the message is responding to. Incorrect `id` value will cause the message to go to wrong request, or be lost. Like `id`, the mandatory 'type' field must also match the request type. Third key `status` indicates the status of the request. On any successful replies, the `status` key contains "ACCEPTED" value. If the `status` key does not contain "ACCEPTED", then the request has failed. (See example below on response to failed request.) Finally, the last key 'data' contains the actual requested data. In this example, it returns a value of "30" as in "30 Celsius" which means the temperature sensor reports that the temperature in the living room is 30 Celsius.

Notice that `next_id` has incremented, the client should take note and use the new value on their next request.

##### Failed Server Response (Resource Not Found)
If the server has determined that the request message is invalid due to no such resource exists on the server, server responds:
```json
{
    "id": 0,
    "type": "REQUEST_SINGLE",
    "status": "ERROR_RESOURCE_NOT_FOUND",
    "next_id": 1
}
```
The structure message is similar to the success message, except that 'data' key is omitted. Like to the successful reply, the 'id' and 'type' must match the request message exactly. The 'status' of this message is "ERROR_RESOURCE_NOT_FOUND" which indicates that the resource does not exist on the server.

##### Failed Server Response (Invalid parameters)
If the server has determined that the request message is invalid due to invalid parameters, server responds:
```json
{
    "id": 0,
    "type": "REQUEST_SINGLE",
    "status": "ERROR_INVALID_PARAMETERS",
    "parameters":{
        "name": "MISSING",
        "unit": "UNKNOWN",
        "offset": "OPTIONAL"
    },
    "next_id": 1
}
```
The message is similar to "ERROR_RESOURCE_NOT_FOUND" error reply. The 'status' of this message is "ERROR_INVALID_PARAMETERS" which indicates that the parameters that was included on the request message were invalid in some way. This message includes a `parameters` key with a map of combined required and included parameters, and their description. The description contains four possible values; "MISSING" which indicates that the parameter is required by the server but is missing on the request message, "UNKNOWN" which indicates that the parameter that is unknown to the server and was included on the request message, "OPTIONAL" which indicates that this unused optional parameter exists, and "OK" which indicates that the parameter is correct.

##### Failed Server Response (Generic Exception)
If the server has determined that the request message is invalid due to an exception being thrown, server responds:
```json
{
    "id": 0,
    "type": "REQUEST_SINGLE",
    "status": "EXCEPTION",
    "message": "Temperature sensor name 'living_room_1' does not exist.",
    "next_id": 1
}
```
The message contains "EXCEPTION" as `status` which indicates that this reply is an exception thrown by the resource. The mandatory `message` key contains a message that is defined by the developer. The message may contain any string value except for `null`.

##### Failed Server Response (Internal Server Error)
If the server has determined that the request message is invalid due to an exception being thrown, server responds:
```json
{
    "id": 0,
    "type": "REQUEST_SINGLE",
    "status": "INTERNAL_SERVER_ERROR",
    "message": "NullPointerException on 'houseId' at line 323 in House.getTemperatureSensor()",
    "next_id": 1
}
```
The message contains "INTERNAL_SERVER_ERROR" as `status` which indicates that the server is experiencing an internal server error. The `message` key contains a message that describes what has caused the error.

### Submitting a streaming request
##### Client Request
To initiate a stream, the client submits:
```json
{
    "id": 1,
    "type": "REQUEST_STREAM",
    "resource": "solar_panel_output",
    "parameters": {
        "sensor_name": "north_roof"
    },
    "lossy": {
        "type": "MESSAGE_LIMIT",
        "limit": 60,
        "interval_unit": "SECOND"
    }
}
```
The message looks similar to single-fire request except that it has a different `type` value of "REQUEST_STREAM" which indicates that this request is requesting for a continuous stream of data updates from the server. The message includes an **optional** `lossy` key. If the `lossy` key is set to `null` in the message, then the request is requesting for a lossless stream, meaning no packets gets dropped in the stream. Not including `lossy` key in the request will automatically default to setting `lossy` to `null`. Defining the optional `lossy` key with a map of proper values indicates that the request is requesting for a lossy streaming, means packets may get dropped in the stream on certain conditions defined by the requester. The `lossy` map must contain `type` key and additional required keys depending on the value of `type` key. See below for all lossy settings.

In this example, the client requests the server to set up a stream on "solar_panel_output" resource. `sensor_name` parameter contains "north_roof" which presumely refers to the name of solar panel located at the north roof of the building.

##### Lossy Setting - Lossless
```json
null
```
Setting the `lossy` to null (or simply drop the key from the request) tells the server to never drop any messages regardless of the amount of messages being sent to the client.

##### Lossy Setting - Message Rate Limit
```json
{
    "type":"MESSAGE_LIMIT",
    "limit": 60,
    "interval_unit": "SECOND"
}
```
"MESSAGE_LIMIT" in the `type` key indicates that the lossy is set to message limit lossy setting. This tells the server to put a cap on the amount of messages that are being streamed to the client. The limit is defined with a number in `limit` and interval is definied in `interval_unit`. `interval_unit` must contain a valid time unit.

This example lossy setting defines a limit of 60 data messages over the stream in a second. If server has exceeded that limit, then those extra data messages gets dropped and the client will never receive them.

##### Lossy Setting - Data Rate Limit
```json
{
    "type":"DATA_LIMIT",
    "limit": 1,
    "unit": "MEGABITS",
    "interval_unit": "SECOND"
}
```
"DATA_LIMIT" in the `type` key indicates that the lossy is set to data rate limit lossy setting. This tells the server to put a cap on the amount of data that are being streamed to the client. The limit is defined with a number in `limit`, unit of data is defined with a type in `unit`, and interval is definied in `interval_unit`. `unit` must contain a valid data size unit. `interval_unit` must contain a valid time unit.

This example lossy setting defines a limit of 1 megabit per stream on over the stream. If server has exceeded that limit, then those extra data gets dropped and the client will never receive them.

##### Lossy Setting - Interval
```json
{
    "type":"INTERVAL",
    "time": 10,
    "unit": "SECOND"
}
```
"INTERVAL" in the `type` key indicates that the lossy is set to use the interval lossy setting. This tells the server to only send message over to the client at a specified interval. The interval time is defined with a number in `time` and interval is definied in `unit`. `unit` must contain a valid time unit.

This example lossy setting defines a message will be sent once in every 10 seconds. If the data has been updated often during the interval, then the most recent data will be sent, the older data are not sent.

##### Successful Server Response
If the server has determined that the message is valid, server responds:
```json
{
    "id": 1,
    "type": "REQUEST_STREAM",
    "status": "ACCEPTED",
    "data": "100W",
    "next_id": 2
}
```
The response message is similar to single-fire response message. There's `data` is **optional** where client may use this data to as an initial data.

##### Server Stream Update
When there's new data, server sends the client a message:
```json
{
    "id": 1,
    "type": "STREAM_UPDATE",
    "data": "101W"
}
```

The `id` refers to the request that has requested for the stream. `type` is "UNI_STREAM_UPDATE" which indicates this data is brand new off the stream. `data` is the brand new date.

In this example, the solar panel's output has increased by 1 Watt and the server is notified of the change and sends a message containing the new value to the client.

It is important to note that `next_id` is omitted in this message as this is an off-request response.

##### Server Stream Terminate
When the server wishes to terminate the stream, server sends the client a message:
```json
{
    "id": 1,
    "type": "STREAM_TERMINATE",
    "code": "NO_MORE_DATA"
}
```
The `id` refers to the request that has requested for the stream. `type` is "UNI_STREAM_UPDATE" which indicates that the server wishes to stop the stream and will not be sending any more data. When the client receives this message, the client should immediately stop using the stream. `code` describes the event. The possible values of `code` are:

* NO_MORE_DATA - Indicates that the stream had to be stopped because there is no more data
* REVOKED - Indicates that the stream was stopped because server has decided to revoke the access to the stream
* TIME_OUT - Indicates that the stream has been timed out by the server
* UNKNOWN - Catch-all code for condition that does not suit to all other codes above **TODO:** Discuss on this code

It is important to note that `next_id` is omitted in this message as this is an off-request response.

##### Client Stream Update
WHen the client wishes to update the stream, client sends a message:
```json
{
    "id": 1,
    "type": "REQUEST_STREAM_UPDATE",
    "parameters": {
        "sensor_name": "south_roof"
    },
    "lossy": {
        "type":"DATA_LIMIT",
        "limit": 1,
        "unit": "MEGABITS",
        "interval_unit": "SECOND"
    }
}
```
The message is similar to the "REQUEST_STREAM" except there is no `resource` and `type` is set to REQUEST_STREAM_UPDATE. This indicates that the client is requesting to update the stream with new parameters. The `lossy` is optional as user may include it to update the lossy settings. Unlike the "REQUEST_STREAM", not including `lossy` in the request does not make the stream to become lossless. Setting `null` to `lossy` will make the stream to become lossless. When the request is submitted, the older stream becomes "dropped" and gets replaced with brand new stream. Essentially this request is similar to making two requests of terminating the stream, then recreating the same stream with new parameters.

##### Client Stream Settings Update
When the client desires to change the `lossy` settings of an already-established stream, the client may send a message:
```json
{
    "id": 1,
    "type": "REQUEST_STREAM_SETTINGS",
    "lossy": null
}
```
The `id` refers to the request that has requested for the stream. `type` is "REQUEST_STREAM_SETTINGS" which indicates this request is a request to change the existing stream. `lossy` is the new lossy settings. `lossy` is a required key.

In this example, the client is requesting to change the `lossy` settings from formerly message rate limit to lossless.

##### Client Stream Terminate
When the client wishes to terminate the stream, client sends a message:
```json
{
    "id": 1,
    "type": "REQUEST_STREAM_TERMINATE"
}
```
The `id` refers to the request that has requested for the stream. `type` is "REQUEST_STREAM_TERMINATE" which indicates that the client wishes to stop receiving the updates from the stream.

##### Failed Server Response (Invalid Lossy Settings)
When the server has determined that the client's request is invalid, server sends a message:
```json
{
    "id": 1,
    "type": "REQUEST_STREAM",
    "status": "ERROR_INVALID_LOSSY_SETTINGS",
    "settings": {
        "type": "OK",
        "interval": "UNKNOWN",
        "unit": "MISSING"
    }
}
```
The message is similar to "ERROR_INVALID_PARAMETERS" error reply. The `status` of this message is "ERROR_INVALID_LOSSY_SETTINGS" which indicates that the values in the lossy settings are invalid in some way. `settings` key is same as "ERROR_INVALID_PARAMETERS"'s `parameters` key.

## Global Settings
##### Global Settings Update
When the client wishes to adjust the global settings to set up a data limit, the client submits a message:
```json
{
    "id": 2
    "type": "SETTINGS_UPDATE",
    "settings":{
        "limit":{
            "type":"DATA_LIMIT",
            "limit": 10,
            "unit": "MEGABITS",
            "interval_unit": "SECOND"
        },
        "binary":{
            "enable":true,
            "base": "base64"
        }
    }
}
```

The message contains `type` key with value of "SETTINGS_UPDATE" which tells the server that the message is for changing the global settings. `settings` key contains a map of all setting values that needs to be configured.

**TODO:** Settings configuration

##### Global Settings Success Response
When the server determines that the global settings update message is valid, server responds:
```json
{
    "id": 2,
    "type": "SETTINGS_UPDATE",
    "status": "ACCEPTED",
    "next_id": 3
}
```

##### Global Settings Error Response
When the server determines that the global settings update message is invalid, server responds:
```json
{
    "id": 2,
    "type": "SETTINGS_UPDATE",
    "status": "INVALID_SETTINGS",
    "settings":{
        "limit":{
            "type": "OK",
            "limit": "UNKNOWN",
            "unit": "INVALID",
            "interval_unit": "MISSING",
            "math": "OPTIONAL"
        },
        "binary" : "UNKNOWN"
    },
    "next_id": 3
}
```
**TODO:** Settings configuration
